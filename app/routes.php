<?php

use App\Controllers\ProfileFacebookController;

$app->get('/profile/facebook/{id:[0-9]+}', ProfileFacebookController::class . ':get');
